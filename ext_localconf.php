<?php  
if (!defined ('TYPO3_MODE')) die ('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(    
	//  unique  plugin  name    
	'OLIGOFORM.' . $_EXTKEY,
	'ContentRenderer',
	//  accessible  controller-­‐action-­‐combinations
	array  (  
		'Elements'  =>  'owlcarousel'
	), 
	//  non-­‐cachable  controller-­‐action-­‐combinations  (they  must  already  be  enabled)
	array  (  
		'Elements'  =>   '' 
		)  
	);  
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY,'setup',
	'[GLOBAL]  
	tt_content.content_owlcarousel  = COA
	tt_content.content_owlcarousel {
	  10 =< lib.stdheader
	  20 =<  tt_content.list.20.contentowlcarousel_contentrenderer
	  20.switchableControllerActions.Elements.1  =  owlcarousel
     }',
	true  );  
