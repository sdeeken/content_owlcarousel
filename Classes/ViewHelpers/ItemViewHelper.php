<?php
namespace OLIGOFORM\ContentOwlcarousel\ViewHelpers;

/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Extbase\Domain\Model\AbstractFileFolder;

/**
 * dieser viewhelper is inspired by the fluid image-viewhelperinspiriert, but returns not an imagetag, but an item for the owl
 *
 * = Examples =
 *
 * <code title="Default">
 * <f:image src="EXT:myext/Resources/Public/typo3_logo.png" alt="alt text" />
 * </code>
 * <output>
 * <img alt="alt text" src="typo3conf/ext/myext/Resources/Public/typo3_logo.png" width="396" height="375" />
 * or (in BE mode):
 * <img alt="alt text" src="../typo3conf/ext/viewhelpertest/Resources/Public/typo3_logo.png" width="396" height="375" />
 * </output>
 *
 * <code title="Image Object">
 * <f:image image="{imageObject}" />
 * </code>
 * <output>
 * <img alt="alt set in image record" src="fileadmin/_processed_/323223424.png" width="396" height="375" />
 * </output>
 *
 */
class ItemViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
	/**
	 * @var \TYPO3\CMS\Extbase\Service\ImageService
	 * @inject
	 */
	protected $imageService;

	/**
	 * Resizes a given image (if required) and renders the respective img tag
	 *
	 * @see http://typo3.org/documentation/document-library/references/doc_core_tsref/4.2.0/view/1/5/#id4164427
	 * @param array $settings typoscript setup 
	 * @param array $data the content object data
	 * @param integer $index imageIteration
	 * @param boolean $treatIdAsReference given src argument is a sys_file_reference record
	 * @param FileInterface|AbstractFileFolder $image a FAL object
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string Rendered tag
	 */
	public function render($settings, $data, $image, $index = 0) {
		$image = $this->imageService->getImage(NULL, $image, NULL);
		
		$width = $data['imagewidth'] > 0 ? $data['imagewidth'] : $settings['image']['maxWidth'];
		
		$height = $data['imageheight'] > 0 ? $height = $data['imageheight'] : $settings['image']['maxHeight'];
		
	    $processingInstructions = array(
			'maxWidth' => $width,
			'maxHeight' => $height,
		);
		$processedImage = $this->imageService->applyProcessingInstructions($image, $processingInstructions);
		$imageUri = $this->imageService->getImageUri($processedImage);
		$width = $processedImage->getProperty('width');
		
		//lightbox?
		if($settings['image']['lightbox'] == 1 || $data['image_zoom'] == 1){
			$lightboxImage = $this->imageService->getImage(NULL, $image, NULL);
			$lBProcessingInstructions = array(
				'maxWidth' => $settings['image']['pp']['maxWidth'],
				'maxHeight' => $settings['image']['pp']['maxHeight'],
			);
			$lBProcessedImage = $this->imageService->applyProcessingInstructions($lightboxImage, $lBProcessingInstructions);
			$prettyPhoto =  $data['image'] != 1 ? 'rel="prettyPhoto[pp_gal' .$data['uid'] .']"' : 'rel="prettyPhoto"';
			$ATagParams = $settings['image']['pp']['ATagParams'] ? $settings['image']['pp']['ATagParams'] : $prettyPhoto;
			
			$lightboxLink1 = '<a href="' . $this->imageService->getImageUri($lBProcessedImage) . '" ' . $ATagParams . '  title="' . $image->getProperty('description') . '">';
			$lightboxLink2 = '</a>';
			
		} else {
			$lightboxLink1 = '';
			$lightboxLink2 = '';
		}
		$unLazyAttr = '';
		$lazyAttr = 'class="lazyOwl" data-';
		//lazy loading? 
		switch ($data['layout']) {
			case 0:
				$lazy = strpos('lazyLoad:true', str_replace(' ','', $settings['owlConf']['default'])) === FALSE ? $unLazyAttr : $lazyAttr ;
				break;
			default:
				$lazy = strpos('lazyLoad:true', str_replace(' ','', $settings['owlConf'][$data['layout']])) === FALSE ? $unLazyAttr : $lazyAttr;
		}
			
		$krempel = '<div class="item"';
		$krempel .= $index == 0 ? ' itemprop="image">' : '>';
		$krempel .= $lightboxLink1 . '<img ' . $lazy . 'src="' . $imageUri . '" alt="' . $image->getProperty('alternative') . '">' . $lightboxLink2;
		
		$krempel .= '</div>';
		return $krempel;
		
	}
}
