<?php
namespace OLIGOFORM\ContentOwlcarousel\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Sabine Deeken <deeken@oligoform.com>, OLIGOFORM
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 
 * @package content_owlcarousel
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ElementsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * action owlcarousel
	 *
	 * @return \string the rendered view
	 */
	public function owlcarouselAction() {
		
		$this->contentObj = $this->configurationManager->getContentObject();
		$data = $this->contentObj->data;
		$this->view->assign('data', $data);
		
		$myExtPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey());
		$owlConf = $data['layout']>0 ? $this->settings['owlConf'][$data['layout']] : $this->settings['owlConf']['default'];
		//inline js in den footer laden
		// 	jquery 1.7+
		if($this->settings['includeJquery'] != '0'){
			 $GLOBALS['TSFE']->additionalFooterData['jquery'] = '<script src="' . $myExtPath . 'Resources/Public/owl-carousel/jquery.min.js"></script>';
		}
		// jsplugin
		if($this->settings['owlConf']['image']['lightbox'] == 1 || $data['image_zoom'] == 1){
		$GLOBALS['TSFE']->additionalFooterData['jquery_owl1_prettyPhoto']=  '<script src="' . $myExtPath . 'Resources/Public/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
		var height = $(window).height();
		var width = $(window).width();

		if ((width>=700) && (height>=500)) { /*fuer kleinere Bildschirme lohnt sich das nicht.*/
		$("a[rel^=\'prettyPhoto\']").prettyPhoto({
			theme: "' . $this->settings['image']['pp']['theme'] . '", 
			social_tools: false
			});
		  }
		  else{
		  $("a[rel^=\'prettyPhoto\']").removeAttr("href");
		  }
		});
		</script>';
		}
		$GLOBALS['TSFE']->additionalFooterData['jquery_owl2_contentOwlcarousel0'] = '<script src="' . $myExtPath . 'Resources/Public/owl-carousel/owl.carousel.js"></script>';
		// inline für die Contentelemente
		$GLOBALS['TSFE']->additionalFooterData['jquery_owl2_contentOwlcarousel1'] = ' <script>$(document).ready(function() {';
		$GLOBALS['TSFE']->additionalFooterData['jquery_owl2_contentOwlcarousel2'] .= '
			$("#owl-cId' . $data['uid'] . '").owlCarousel({
				' . $owlConf . '
			});
		';
		$GLOBALS['TSFE']->additionalFooterData['jquery_owl2_contentOwlcarousel3'] = ' });</script>';
		
	}

}
?>
