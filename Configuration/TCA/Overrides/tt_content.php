<?php

/***************
 * Add Content Elements to List
 */
$backupCTypeItems = $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'] = array(
    array(
        'Owl-Carousel',
        'content_owlcarousel',
        'i/tt_content_header.gif'
    ),
);
foreach ($backupCTypeItems as $key => $value) {
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = $value;
}
unset($key);
unset($value);
unset($backupCTypeItems);

$TCA['tt_content']['types']['content_owlcarousel']['showitem']  =  $TCA['tt_content']['types']['textpic']['showitem'];
//das erstmal nicht subheader ungeeignet . ',--div--;owlsliderconf, subheader;jswlslider';
